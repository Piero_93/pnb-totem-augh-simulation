package pnb.totem;

import java.util.Set;
import java.util.stream.Collectors;

import it.unibo.alchemist.model.implementations.molecules.SimpleMolecule;
import it.unibo.alchemist.model.implementations.neighborhoods.CachedNeighborhood;
import it.unibo.alchemist.model.interfaces.Environment;
import it.unibo.alchemist.model.interfaces.LinkingRule;
import it.unibo.alchemist.model.interfaces.Molecule;
import it.unibo.alchemist.model.interfaces.Neighborhood;
import it.unibo.alchemist.model.interfaces.Node;

public class PNBNetworkModel<T> implements LinkingRule<T> {
	private static final long serialVersionUID = 1L;

	private double range;
	
	private Molecule totem;
	private Molecule app;

	public PNBNetworkModel(double range) {
		super();
		this.range = range;
		
		totem = new SimpleMolecule("totem");
		app = new SimpleMolecule("app");
	}

	@Override
	public Neighborhood<T> computeNeighborhood(Node<T> center, Environment<T> env) {
		Set<Node<T>> allNeighbors = env.getNodesWithinRange(center, range); 
		
		Set<Node<T>> neighbors = allNeighbors.stream().filter((c) -> {
			return (center.contains(totem) && !c.contains(totem)) ||
					(center.contains(app) && !c.contains(app)) ||
					(!center.contains(app) && !center.contains(totem));
		}).collect(Collectors.toSet());
		
		return new CachedNeighborhood<>(center, neighbors, env);
	}

	@Override
	public boolean isLocallyConsistent() {
		return true;
	}
	

}
